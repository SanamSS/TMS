﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TMS.DAL;
using TMS.ViewModels;

namespace TMS.Controllers
{
    public class TasksController : Controller
    {
        Dbconn ab = new Dbconn();


        // GET: Role
        public ActionResult Index()
        {
            var FriendList = new List<TasksViewModel>();
            var conn = ab.ConnStrg();

            FriendList =
                conn.Query<TasksViewModel>("select t.Task_Id, t.task_name, t.task_description, t.task_createdon, t.task_assigneddays, ts.Task_Status_Name, ts.Task_Priority , u.Name AS Task_CreatedBy_User_Name, us.Name AS Task_AssignedTo_User_Name, t.Task_CreatedBy_User_Id, t.Task_AssignedTo_User_Id from dbo.tasks t  join dbo.users u ON t.task_createdby_user_id = u.user_id   join dbo.task_status ts ON t.Task_Status_Id = ts.Task_Status_Id  join dbo.users us  ON t.Task_AssignedTo_User_Id = us.user_id").ToList();

            return View(FriendList);
        }


        // GET: Friend/Create  
        public ActionResult Create()
        {
            TasksViewModel uvm = new TasksViewModel();
            var conn = ab.ConnStrg();
            var vm = conn.Query<UsersViewModel>("Select User_Id, Name from users").ToList();
            uvm.Task_AssignedTo_User_Names = from u in vm.ToList()
                             select new SelectListItem
                             {
                                 Text = u.Name,
                                 Value = u.User_Id.ToString()
                             };

            var vm1 = conn.Query<Task_Status_ViewModel>("Select Task_Status_Id, Task_Status_Name from task_status").ToList();
            uvm.Task_Status_Names = from u in vm1.ToList()
                                             select new SelectListItem
                                             {
                                                 Text = u.Task_Status_Name,
                                                 Value = u.Task_Status_Id.ToString()
                                             };





            return View(uvm);



        }

        // POST: Friend/Create  
        [HttpPost]
        public ActionResult Create(TasksViewModel t)
        {

            DynamicParameters para = new DynamicParameters();
            para.Add("task_name",t.Task_Name);
            para.Add("@task_description",t.Task_Description);
            para.Add("@task_createdby_user_id", t.Task_CreatedBy_User_Id);
            para.Add("@task_assignedto_user_id", t.Task_AssignedTo_User_Id);
            para.Add("@@task_createdon", t.Task_CreatedOn);
            para.Add("@task_assigneddays", t.Task_AssignedDays);
            para.Add("@task_status_id", t.Task_Status_Id);
            para.Add("@task_status_Name", t.Task_Status_Name);
            para.Add("@task_priority", t.Task_Priority);
            para.Add("@task_status_createdby_user_id", t.Task_CreatedBy_User_Id);

            var conn = ab.ConnStrg();

            conn.Open();
            conn.Execute("InsertINTOTASKS_AND_Task_STATUS", para,commandType: CommandType.StoredProcedure);
          conn.Close();

            return RedirectToAction("Index");
        }




    }
}