﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TMS.DAL;
using TMS.ViewModels;

namespace TMS.Controllers
{
    public class UserController : Controller
    {
        Dbconn ab = new Dbconn();
        // GET: User
        public ActionResult Index()
        {
            var FriendList = new List<UsersViewModel>();
            var conn = ab.ConnStrg();

            FriendList = conn.Query<UsersViewModel>("select u.name,u.email,r.role_name from dbo.users u join dbo.role r ON r.role_id = u.role_id").ToList();

            return View(FriendList);
        }


        // GET: Friend/Create  
        public ActionResult Create()
        {
            UsersViewModel uvm = new UsersViewModel();
            var conn = ab.ConnStrg();
            var vm = conn.Query<RoleViewModel>("Select Role_id, Role_Name from role").ToList();
            uvm.Role_Names = from u in vm.ToList() select new SelectListItem {
                Text=u.Role_Name,Value=u.Role_Id.ToString()
            };

            return View(uvm);


           
        }

        // POST: Friend/Create  
        [HttpPost]
        public ActionResult Create(UsersViewModel _friend)
        {



            string sqlQuery = "Insert Into Users(Name,Email,Password,Role_Id) Select '" + _friend.Name + "','" + _friend.Email + "','" + _friend.Password + "'," + _friend.Role_Id + "";
            var conn = ab.ConnStrg();

            int rowsAffected = conn.Execute(sqlQuery);


            return RedirectToAction("Index");
        }


    }
}