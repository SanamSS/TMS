﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TMS.DAL;
using TMS.ViewModels;

namespace TMS.Controllers
{
    public class RoleController : Controller
    {
        Dbconn ab = new Dbconn();

      
        // GET: Role
        public ActionResult Index()
        {
           var FriendList = new List<RoleViewModel>();
            var conn = ab.ConnStrg();

            FriendList = conn.Query<RoleViewModel>("Select * From dbo.role").ToList();
            
            return View(FriendList);
        }

        // GET: Friend/Create  
        public ActionResult Create()
        {

            return View();
        }

        // POST: Friend/Create  
        [HttpPost]
        public ActionResult Create(RoleViewModel _friend)
        {

          

                string sqlQuery = "Insert Into role (Role_Name) select ('" + _friend.Role_Name + "')";
            var conn = ab.ConnStrg();

            int rowsAffected = conn.Execute(sqlQuery);
            

            return RedirectToAction("Index");
        }

        
        // GET: Friend/Edit/5  
        public ActionResult Edit(int id)
        {

            var conn = ab.ConnStrg();
            RoleViewModel _friend = new RoleViewModel();
           
                _friend = conn.Query<RoleViewModel>("Select * From role " +
                                       "WHERE Role_Id =" + id, new { id }).SingleOrDefault();
           
            return View(_friend);
        }

        // POST: Friend/Edit/5  
        [HttpPost]
        public ActionResult Edit(RoleViewModel _friend)
        {
            var conn = ab.ConnStrg();

            string sqlQuery = "update role set Role_Name='" + _friend.Role_Name+ "' where Role_Id=" + _friend.Role_Id;

                int rowsAffected = conn.Execute(sqlQuery);
            

            return RedirectToAction("Index");
        }
        // GET: Friend/Delete/5  
        public ActionResult Delete(int id)
        {
            var conn = ab.ConnStrg();
            RoleViewModel _friend = new RoleViewModel();
          
                _friend = conn.Query<RoleViewModel>("Select * From Role " +
                                       "WHERE Role_Id =" + id, new { id }).SingleOrDefault();
            
            return View(_friend);
        }

        // POST: Friend/Delete/5  
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            var conn = ab.ConnStrg();
            string sqlQuery = "Delete From role WHERE Role_Id = " + id;

                int rowsAffected = conn.Execute(sqlQuery);


            
            return RedirectToAction("Index");
        }


    }
}