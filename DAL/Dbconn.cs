﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TMS.DAL
{
    public class Dbconn
    {
        public IDbConnection  ConnStrg()
        {

            IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["TMSDBContext"].ConnectionString);

            return db;
    }
         
    }
}