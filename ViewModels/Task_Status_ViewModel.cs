﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMS.ViewModels
{
    public class Task_Status_ViewModel
    {
        public int Task_Status_Id { get; set; }
        public string Task_Status_Name { get; set; }
        public string Task_Priority { get; set; }
        public int Task_Status_CreatedBy_User_Id { get; set; }
     

    }
}