﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TMS.ViewModels
{

    public class UsersViewModel
    {
        [Key]
        public int User_Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int? Role_Id { get; set; }

        public string Role_Name { get; set; }
        public  IEnumerable<SelectListItem> Role_Names { get; set; }
    }
}