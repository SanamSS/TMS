﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TMS.ViewModels
{
  

        public class RoleViewModel
        {


            [Key]
            public int Role_Id { get; set; }
            public string Role_Name { get; set; }

        }
    
}