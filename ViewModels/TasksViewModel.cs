﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TMS.ViewModels
{
    public class TasksViewModel
    {

        public int Task_Id { get; set; }
        public string Task_Name { get; set; }
        public string Task_Description { get; set; }

        public int? Task_CreatedBy_User_Id { get; set; }
        public string Task_CreatedBy_User_Name { get; set; }
        public int? Task_AssignedTo_User_Id { get; set; }

        public string Task_AssignedTo_User_Name { get; set; }
        public DateTimeOffset Task_CreatedOn { get; set; }
        public int Task_AssignedDays { get; set; }

        public int? Task_Status_Id { get; set; }
        public string Task_Status_Name { get; set; }
        public string Task_Priority { get; set; }

        public IEnumerable<SelectListItem> Task_AssignedTo_User_Names { get; set; }
        public IEnumerable<SelectListItem> Task_Status_Names { get; set; }
    }
}